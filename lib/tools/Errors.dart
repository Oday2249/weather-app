import 'package:equatable/equatable.dart';

import 'DayInfo.dart';

class ErrorMessage extends Equatable {

  String message;

  ErrorMessage({required this.message});

  @override
  List<Object> get props => [];
}


class CashReadException implements Exception {}
class CashRegisterException implements Exception {}

class CloudReadException implements Exception {}
class HTTPCloudreadException implements Exception {}

class ErrorMessages{

  static String CASH_READ_ERROR = "cash read error";
  static String CASH_REGISTER_ERROR = "cash register error";
  static String SERVER_ERROR = "  server error\n(check API key)";

}
