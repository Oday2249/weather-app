class DatesOp {
  static List<String> generateDates(String dateStr) {
    int DAYS = 5;
    List<String> dates = <String>[];
    List<String> DMY = dateStr.split(" ")[0].split("-");
    String hours = dateStr.split(" ")[1].split(":")[0];
    print("hours ${hours}");
    var date =
    new DateTime(int.parse(DMY[0]), int.parse(DMY[1]), int.parse(DMY[2]), int.parse(hours));
    for (int i = 1; i < DAYS; i++) {
      dates.add(new DateTime(date.year, date.month, date.day + i, int.parse(hours)).toString());
    }

    return dates;
  }

  static bool DateTimeCompare(String dateA, String dateB){

    print("dateA ${dateA}");
    print("dateB ${dateB}");

    print("day to compareA ${int.parse(dateA.split(" ")[0].split("-")[2])}");
    print("day to compareB ${int.parse(dateB.split(" ")[0].split("-")[2])}");

    print("time to compareA ${int.parse(dateA.split(" ")[1].split(":")[0])}");
    print("time to compareB ${int.parse(dateB.split(" ")[1].split(":")[0])}");

    if(int.parse(dateA.split(" ")[0].split("-")[2]) != int.parse(dateB.split(" ")[0].split("-")[2])){
      return false;
    }else if((int.parse(dateA.split(" ")[1].split(":")[0]) - int.parse(dateB.split(" ")[1].split(":")[0])).abs() > 2){

      return false;
    }
    else return true;

  }
}
