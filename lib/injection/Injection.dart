import 'package:weather_app/business_logic/repositories/WeatherRepository.dart';
import 'package:weather_app/business_logic/use_cases/CurrentWeatherUseCase.dart';
import 'package:weather_app/business_logic/use_cases/WeatherForcastUseCase.dart';
import 'package:weather_app/data_access/data_source_readers/CashReader.dart';
import 'package:weather_app/data_access/data_source_readers/CloudReader.dart';
import 'package:weather_app/data_access/reps_imp/WeatherRepoDef.dart';
import 'package:weather_app/presentation/bloc/WeatherDetailScreenBLoC.dart';
import 'package:weather_app/presentation/states/States.dart';



class Injection{

  static final WDSBloc Instance = BuilDependencies();



  static WDSBloc BuilDependencies() {

    Cash cash = SharedPrefsBasedCash();
    CloudReader cloudReader = HTTPCloudReader();


    WeatherRepository weatherRepository = WeatherRepositoryDef(
        cash: cash,
        cloudReader: cloudReader,);

    CurrentWeatherUseCase currentWeatherUseCase = CurrentWeatherUseCase(repo: weatherRepository);
    ForecastWeatherUseCase forecastWeatherUseCase = ForecastWeatherUseCase(repo: weatherRepository);

    WDSBloc bloc = WDSBloc(LoadingWeatherInfo(), currentWeatherUseCase, forecastWeatherUseCase);

    return bloc;
  }

  static WDSBloc BuilNewDependencies() {

    WDSBloc bloc = WDSBloc(LoadingWeatherInfo(), Instance.currentWeatherUseCase, Instance.forecastWeatherUseCase);

    return bloc;
  }


}
