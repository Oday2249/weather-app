import 'package:weather_app/tools/DayInfo.dart';

abstract class event{}

class forecastWeatherDisplayEvent extends event {
  DayInfo dayInfo;
  forecastWeatherDisplayEvent({required this.dayInfo});
}

class currentDayWeatherDisplayEvent extends event {
  currentDayWeatherDisplayEvent();
}
