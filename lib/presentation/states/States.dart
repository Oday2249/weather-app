
import 'package:weather_app/business_logic/data_models/WeatherInfo.dart';

abstract class state{

}

class LoadingWeatherInfo extends state {}
class DisplayErrorMessage extends state {
  String message;
  DisplayErrorMessage({required this.message});
}


class DisplayCurrentWeatherInfo extends state {
  List<WeatherInfo> weatherInfo;
  DisplayCurrentWeatherInfo({required this.weatherInfo});
}

class DisplayForecastWeatherInfo extends state {
  WeatherInfo weatherInfo;
  DisplayForecastWeatherInfo({required this.weatherInfo});
}
