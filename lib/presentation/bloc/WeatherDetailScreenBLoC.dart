import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/business_logic/use_cases/CurrentWeatherUseCase.dart';
import 'package:weather_app/business_logic/use_cases/WeatherForcastUseCase.dart';
import 'package:weather_app/presentation/events/Events.dart';
import 'package:weather_app/presentation/states/States.dart';
import 'dart:io';

import 'package:weather_app/tools/DayInfo.dart';

class WDSBloc extends Bloc<event, state> {
  WDSBloc(state initialState, CurrentWeatherUseCase currentWeatherUseCase,
      ForecastWeatherUseCase forecastWeatherUseCase)
      : super(initialState) {
    this.currentWeatherUseCase = currentWeatherUseCase;
    this.forecastWeatherUseCase = forecastWeatherUseCase;
  }

  late CurrentWeatherUseCase currentWeatherUseCase;
  late ForecastWeatherUseCase forecastWeatherUseCase;

  //WDSBloc(state initialState) : super(LoadingWeatherInfo());

  @override
  state get initialState => LoadingWeatherInfo();

  @override
  Stream<state> mapEventToState(event e) async* {
    // update state : state(t + 1) = f(state(t), Event)
    print("call bloc");
    if (e is currentDayWeatherDisplayEvent) {
      yield LoadingWeatherInfo();
      final result = await currentWeatherUseCase.get(DayInfo(day: 0, date: "_"));
      yield* result.fold(
            (errorMessage) async* {
              print("error in currentWeatherUseCase");
              yield DisplayErrorMessage(message: errorMessage.message);
        },
            (weatherInfo) async* {
              print("success in currentWeatherUseCase");
              yield DisplayCurrentWeatherInfo(weatherInfo: weatherInfo);
        },
      );// should make request here

    } else if (e is forecastWeatherDisplayEvent) {
      print("i should load");
      yield LoadingWeatherInfo();
      final result = await forecastWeatherUseCase.get(e.dayInfo);
      yield* result.fold(
            (errorMessage) async* {
          print("error in forcastWeatherUseCase");
          yield DisplayErrorMessage(message: errorMessage.message);
        },
            (weatherInfo) async* {
          print("success in forcastWeatherUseCase");
          yield DisplayForecastWeatherInfo(weatherInfo: weatherInfo);
        },
      );// should make request here

    }
  }
}
