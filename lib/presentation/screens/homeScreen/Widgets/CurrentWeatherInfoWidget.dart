import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:weather_app/presentation/states/States.dart';

class CurrentWeatherInfo extends StatelessWidget {

  late double screenWidth;
  late double screenHeight;
  late DisplayCurrentWeatherInfo state;

  CurrentWeatherInfo({required this.screenWidth, required this.screenHeight, required this.state});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth,
      height: screenHeight * (1.8/3),
      padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.1, vertical: screenHeight * (1.8/3) * 0.1),
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
               Container(
                 height: screenHeight * (1.8/3) * 0.1,
                 child: Center(
                   child: Text(
                     state.weatherInfo[0].city,
                     style: TextStyle(
                       color: Colors.black54,
                       fontSize: 23.0
                     ),
                   ),
                 ),
               ),
              Container(
                height: screenHeight * (1.8/3) * 0.1,
                child: Center(
                  child: Text(
                    "${DateFormat('EEEE').format(DateTime.parse(state.weatherInfo[0].date))} ${state.weatherInfo[0].date.split(" ")[0]}",
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: 23.0
                    ),
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: screenHeight * (1.8/3) * 0.1,
                child: Center(
                  child: Text(
                    state.weatherInfo[0].weatherPerTime[0].state,
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: 28.0,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
              Container(
                height: screenHeight * (1.8/3) * 0.1,
                child: Center(
                  child: Text(
                    "${(state.weatherInfo[0].weatherPerTime[0].tempmin - 273.15).round()}/${(state.weatherInfo[0].weatherPerTime[0].tempmax - 273.15).round()}",
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: 28.0,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[

              Container(
                width: screenWidth * 0.4,
                height: screenWidth * 0.4,//screenHeight * (1.8/3) * 0.6,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        "assets/icons/${state.weatherInfo[0].weatherPerTime[0].icon}.png"),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Container(
                width: screenWidth * 0.4,
                height: screenHeight * (1.8/3) * 0.6,
                child: Center(
                  child: Text(
                    "${(state.weatherInfo[0].weatherPerTime[0].tempmin - 273.15).round()}",
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: 100.0,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),

            ],
          )
        ],
      ),
    );
  }
}