import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:weather_app/presentation/bloc/WeatherDetailScreenBLoC.dart';
import 'package:weather_app/presentation/events/Events.dart';
import 'package:weather_app/presentation/screens/weatherDetailScreen/WeatherDetailScreen.dart';
import 'package:weather_app/presentation/states/States.dart';
import 'package:weather_app/tools/DayInfo.dart';

class DaysForecastCasual extends StatelessWidget {
  late double screenWidth;
  late double screenHeight;
  late List<String> dates;
  late state allDays;

  late WDSBloc BlocSink;

  DaysForecastCasual(
      {required this.screenWidth,
      required this.screenHeight,
      required this.dates,
      required this.allDays});

  @override
  Widget build(BuildContext context) {
    BlocSink = BlocProvider.of<WDSBloc>(context);

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          width: screenWidth,
          height: screenHeight * (0.75 / 4) * (1.2 / 3),
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'next 4-day Forecast',
                  style: TextStyle(
                    fontSize: 24.0,
                    color: Colors.black54,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox()
              ],
            ),
          ),
        ),
        Container(
          width: screenWidth,
          height: screenHeight * (2.95 / 4) * (1.2 / 3),
          color: Colors.white,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: 4,
              itemBuilder: (BuildContext context, int Index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => WeatherDetailScreen(
                              dayInfo:
                                  DayInfo(day: Index + 1, date: dates[Index]))),
                    );
                  },
                  child: Container(
                    margin:
                        EdgeInsets.all(screenHeight * (0.1 / 4) * (1.2 / 3)),
                    width: screenWidth * (1 / 5),
                    height: screenHeight * (2.75 / 4) * (1.2 / 3),
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(screenWidth * (1 / 12)),
                      color: Colors.black12,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(
                          DateFormat('EEEE')
                              .format(DateTime.parse(dates[Index])),
                          style: TextStyle(
                            fontSize: 10.0,
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          dates[Index].split(" ")[0],
                          style: TextStyle(
                            fontSize: 14.0,
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Container(
                          width: 40.0,
                          height: 40.0,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(
                                  'assets/icons/${(allDays as DisplayCurrentWeatherInfo).weatherInfo[Index].weatherPerTime[0].icon}.png'),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Text(
                          dates[Index].split(" ")[1].substring(0, 5),
                          style: TextStyle(
                            fontSize: 14.0,
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }),
        ),
        SizedBox(
          height: screenHeight * (0.3 / 4) * (1.2 / 3),
        )
      ],
    );
  }
}
