import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/injection/Injection.dart';
import 'package:weather_app/presentation/bloc/WeatherDetailScreenBLoC.dart';
import 'package:weather_app/presentation/events/Events.dart';
import 'package:weather_app/presentation/states/States.dart';
import 'package:weather_app/tools/DateOp.dart';

import 'Widgets/CurrentWeatherInfoWidget.dart';
import 'Widgets/DaysForecastCasualWidget.dart';

class HomeScreen extends StatelessWidget {
  late WDSBloc BlocSink;

  Widget stateBuilder(state State, double ScreenWidth, double ScreenHeight) {
    if (State is LoadingWeatherInfo) {
      print("LoadingWeatherInfo");
      return Scaffold(
        body: Container(
          width: ScreenWidth,
          height: ScreenHeight,
          color: Colors.white,
          child: Center(
            child: CircularProgressIndicator(),
          ),
        ),
      );
    } else if (State is DisplayCurrentWeatherInfo) {
      print(State.weatherInfo);
      List<String> Dates = DatesOp.generateDates(State.weatherInfo[0].date);
      for (int i = 0; i < Dates.length; i++) {
        print("date ${i} : ${Dates[i]}");
      }
      print("DisplayWeatherInfo");
      return Scaffold(
        body: Container(
          child: Column(
            children: <Widget>[
              CurrentWeatherInfo(
                screenWidth: ScreenWidth,
                screenHeight: ScreenHeight,
                state: State,
              ),
              DaysForecastCasual(
                screenWidth: ScreenWidth,
                screenHeight: ScreenHeight,
                dates: Dates,
                allDays: State,
              )
            ],
          ),
        ),
      );
    } else if (State is DisplayErrorMessage) {
      return Scaffold(
          body: Container(
        child: Center(
          child: Text((State as DisplayErrorMessage).message,
          style: TextStyle(
            fontSize: 30.0,
            fontWeight: FontWeight.bold
          ),),
        ),
      ));
    } else {
      return Scaffold(
        body: Container(
          width: ScreenWidth,
          height: ScreenHeight,
          color: Colors.white,
          child: Center(
            child: Text("no page"),
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    double ScreenWidth = MediaQuery.of(context).size.width;
    double ScreenHeight = MediaQuery.of(context).size.height;

    return BlocProvider<WDSBloc>(
        create: (context) => Injection.Instance,
        child: Builder(builder: (BuildContext context) {
          BlocSink = BlocProvider.of<WDSBloc>(context);

          BlocSink.add(currentDayWeatherDisplayEvent());
          return BlocBuilder<WDSBloc, state>(builder: (context, State) {
            return WillPopScope(
              child: stateBuilder(State, ScreenWidth, ScreenHeight),
              onWillPop: () {
                return Future.value(false);
              },
            );
          });
        }));
  }
}

/*




 */
