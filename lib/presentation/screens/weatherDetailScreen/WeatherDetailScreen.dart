import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/injection/Injection.dart';
import 'package:weather_app/presentation/bloc/WeatherDetailScreenBLoC.dart';
import 'package:weather_app/presentation/events/Events.dart';
import 'package:weather_app/presentation/reusable_widgets/WeatherInfoWidget.dart';
import 'package:weather_app/presentation/states/States.dart';
import 'package:weather_app/tools/DayInfo.dart';

import 'Widgets/WeatherDataListWidget.dart';


class WeatherDetailScreen extends StatelessWidget {
  late WDSBloc BlocSink;
  late DayInfo dayInfo;

  WeatherDetailScreen({required this.dayInfo});


  Widget stateBuilder(state State, double ScreenWidth, double ScreenHeight) {

    if(State is LoadingWeatherInfo){
      print("LoadingWeatherInfo");
      return Scaffold(
        body: Container(
          width: ScreenWidth,
          height: ScreenHeight,
          color: Colors.white,
          child: Center(
            child: CircularProgressIndicator(),
          ),
        ),
      );
    }else if(State is DisplayForecastWeatherInfo){
      print(State.weatherInfo);
      print("forecastWeatherDisplayEvent");
      return Scaffold(
        body: Container(
          child: Column(
            children: <Widget>[
              WeatherInfo(screenWidth : ScreenWidth, screenHeight : ScreenHeight, weatherstate: State),
              WeatherDataList(screenWidth : ScreenWidth, screenHeight : ScreenHeight, weatherstate: State,)
            ],
          ),
        ),
      );
    }else if (State is DisplayErrorMessage){
      print("here i am");
      return Scaffold(body: Container(child: Center(
        child: Text((State as DisplayErrorMessage).message),
      ),));
    }else{

      return Scaffold(
        body: Container(
          width: ScreenWidth,
          height: ScreenHeight,
          color: Colors.white,
          child: Center(
            child: Text(
                "no page"
            ),
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    double ScreenWidth = MediaQuery.of(context).size.width;
    double ScreenHeight = MediaQuery.of(context).size.height;

    return BlocProvider<WDSBloc>(
        create: (context) => Injection.BuilNewDependencies(),
        child: Builder(builder: (BuildContext context) {
          BlocSink = BlocProvider.of<WDSBloc>(context);
          print("displatch forecastWeatherDisplayEvent");
          BlocSink.add(forecastWeatherDisplayEvent(dayInfo: dayInfo));
          return BlocBuilder<WDSBloc, state>(builder: (context, State) {
            return WillPopScope(onWillPop: (){
              Navigator.of(context).pop();
              return Future.value(false);
            },
                child: stateBuilder(State, ScreenWidth, ScreenHeight));
          });

        }));
  }
}


