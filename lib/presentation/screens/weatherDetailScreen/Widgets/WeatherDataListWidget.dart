import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:weather_app/presentation/states/States.dart';

class WeatherDataList extends StatelessWidget {
  late double screenWidth;
  late double screenHeight;
  late state weatherstate;

  WeatherDataList(
      {required this.screenWidth,
      required this.screenHeight,
      required this.weatherstate});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth,
      height: screenHeight * (1.2 / 3),
      color: Colors.white,
      margin: EdgeInsets.symmetric(horizontal: screenWidth * 0.03),
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: 7,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: EdgeInsets.symmetric(vertical: screenWidth * (0.01)),
              width: screenWidth * (0.98),
              height: screenHeight * (0.1),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                  color: Colors.black12
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        DateFormat('EEEE')
                            .format(DateTime.parse((weatherstate as DisplayForecastWeatherInfo).weatherInfo.date)),
                        style: TextStyle(
                          fontSize: 15.0,
                          color: Colors.black54,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        (weatherstate as DisplayForecastWeatherInfo)
                            .weatherInfo
                            .weatherPerTime[index + 1].time,
                        style: TextStyle(
                          fontSize: 15.0,
                          color: Colors.black54,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),

                  Container(
                    width: 40.0,
                    height: 40.0,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                            'assets/icons/${(weatherstate as DisplayForecastWeatherInfo)
                                .weatherInfo
                                .weatherPerTime[index + 1].icon}.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Text(
                    (weatherstate as DisplayForecastWeatherInfo)
                        .weatherInfo
                        .weatherPerTime[index + 1]
                        .state,
                    style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.black54,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    '${((weatherstate as DisplayForecastWeatherInfo).weatherInfo.weatherPerTime[index + 1].tempmin - 273.15).round()}/${((weatherstate as DisplayForecastWeatherInfo).weatherInfo.weatherPerTime[index + 1].tempmax - 273.15).round()}',
                    style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.black54,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            );
          }),
    );
  }
}
