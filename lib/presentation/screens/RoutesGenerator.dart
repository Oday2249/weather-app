import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:weather_app/presentation/screens/weatherDetailScreen/WeatherDetailScreen.dart';
import 'package:weather_app/tools/DayInfo.dart';

import 'homeScreen/HomeScreen.dart';


class RouteGenerator{

  static const String HomeScreenRoute = "/home";
  static const String WeatherDetailScreenRoute = "/weather_detail";

  RouteGenerator._(){}

  static Route<dynamic> generateRoute(RouteSettings settings){

    switch(settings.name){
      case HomeScreenRoute:
        return MaterialPageRoute(builder: (_){
          return HomeScreen();
        });
      case WeatherDetailScreenRoute:
        return MaterialPageRoute(builder: (_){
          return WeatherDetailScreen(dayInfo: DayInfo(date: '_', day: 0));
        });
      default:
        throw FormatException("route is not found");

    }

  }

}