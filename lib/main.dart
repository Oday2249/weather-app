import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_app/data_access/data_source_readers/CloudReader.dart';
import 'package:weather_app/presentation/screens/RoutesGenerator.dart';
import 'package:weather_app/presentation/screens/homeScreen/HomeScreen.dart';


Future<void> SPInit() async {
  SharedPreferences SPO;

  try {
    SPO = await SharedPreferences.getInstance();
    SPO.getString("app-name");
  } catch (err) {
    SharedPreferences.setMockInitialValues({});
    SPO = await SharedPreferences.getInstance();
    SPO.setString("app-name", "my-app");
  }

  var res = SPO.setString("obj", "empty");
}

void main() async{

  await SPInit();
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Weather app',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: AuthScreen(),
    );
  }
}

class AuthScreen extends StatelessWidget{

  late TextEditingController FController = new TextEditingController();


  @override
  Widget build(BuildContext context) {

    double ScreenWidth = MediaQuery.of(context).size.width;
    double ScreenHeight = MediaQuery.of(context).size.height;

    return Scaffold(

      body: Container(
        width: ScreenWidth,
        height: ScreenHeight,
        child: Center(
          child: Container(

            width: ScreenWidth,
            height: 0.5 * ScreenHeight,
            child: Column(
              children: <Widget>[
                Text(
                  "Enter API key",
                  style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold
                  ),
                ),
                Container(
                  width: 0.9 * ScreenWidth,
                  height: 60.0,

                  child: Center(
                    child: TextField(
                      controller: FController,
                      decoration: InputDecoration(

                          hintText: "api key",
                          hintStyle: TextStyle(fontWeight: FontWeight.w300, color: Colors.black)
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20.0,),
                GestureDetector(
                  onTap: (){
                      print(FController.text);
                      HTTPCloudReader.APIKey = FController.text;
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => HomeScreen()),
                      );
                  },
                  child: Container(
                    width: 0.5 * ScreenWidth,
                    height: 60.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30.0),
                        color: Colors.blue
                    ),
                    child: Center(
                      child: Text(
                        "Submit",
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  ),
                )
              ]
            )
          )
        )

      )
    );
  }
}
