import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_app/data_access/data_models/PackUnpackModel.dart';
import 'package:weather_app/tools/DateOp.dart';
import 'package:weather_app/tools/DayInfo.dart';
import 'package:weather_app/tools/Errors.dart';

abstract class Cash {
  Future<int> getAvailablity(DayInfo day);

  Future<dynamic> getRegistered();

  Future<void> register(String data);

}

class SharedPrefsBasedCash implements Cash {

  @override
  Future<dynamic> getRegistered() async {
    SharedPreferences SPO = await SharedPreferences.getInstance();
    var res = SPO.getString("obj") ?? '';
    return Future.value(res);
  }

  @override
  Future<bool> register(String weatherData) async {
    SharedPreferences SPO = await SharedPreferences.getInstance();
    SPO.setString("obj", weatherData);

    return Future.value(true);
  }



  @override
  Future<int> getAvailablity(DayInfo day) async {
    SharedPreferences SPO = await SharedPreferences.getInstance();
    String res = SPO.getString("obj").toString();
    print(res);
    if(res == null){
      return -1;
    }
    if (res == "empty" || res == "") {
      print("no cashing because res is ${res} (no preferences yet)");
      return -1;
    } else {

      DateTime now = DateTime.now();
      DateTime date_Curr =
      new DateTime(now.year, now.month, now.day + day.day, now.hour);
      if (DatesOp.DateTimeCompare(date_Curr.toString(), PackUnpackWeatherData.fromJson(jsonDecode(res), day.day).date)) {
        print("cash matching : ${date_Curr.toString()}, ${PackUnpackWeatherData.fromJson(jsonDecode(res), day.day).date}");
        return day.day;
      }
      print("no cashing because no matches");
      return -1;
    }
  }
}
