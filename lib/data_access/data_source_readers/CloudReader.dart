import 'dart:convert';

import 'package:weather_app/data_access/data_models/PackUnpackModel.dart';
import 'package:weather_app/tools/DayInfo.dart';
import 'package:weather_app/tools/Errors.dart';
import 'package:http/http.dart' as http;

abstract class CloudReader {
  Future<List<PackUnpackWeatherData>> getCurrentWeatherData();

  Future<PackUnpackWeatherData> getForecastWeatherData(DayInfo dayInfo);

  Future<String> getStr();
}

class HTTPCloudReader implements CloudReader {
  bool success() {
    return true;
  }

  static late String APIKey;


  Future<dynamic> get() async {
    print("key: ${APIKey}");

    //Pass headers below
    var response =  await http.get(
        Uri.https("community-open-weather-map.p.rapidapi.com", "forecast",
            {"q": "dubai,uae"}),
        headers: {
          "x-rapidapi-host": "community-open-weather-map.p.rapidapi.com",
          "x-rapidapi-key" : APIKey
        });

    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode >= 400) {
      throw new HTTPCloudreadException();
    }
    Map<String, dynamic> weatherData = jsonDecode(response.body);
    print("request length : ${weatherData['list'][0]['weather'][0]['main']}");

    return weatherData;
  }

  @override
  Future<String> getStr() async {

    print("key: ${APIKey}");

    //Pass headers below
    var response =  await http.get(
        Uri.https("community-open-weather-map.p.rapidapi.com", "forecast",
            {"q": "dubai,uae"}),
        headers: {
          "x-rapidapi-host": "community-open-weather-map.p.rapidapi.com",
          "x-rapidapi-key" : APIKey
        });

    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode >= 400) {
      throw new HTTPCloudreadException();
    }
    Map<String, dynamic> weatherData = jsonDecode(response.body);
    print("request length : ${weatherData['list'][0]['weather'][0]['main']}");

    return response.body;
  }

  @override
  Future<List<PackUnpackWeatherData>> getCurrentWeatherData() async {
    return await getAll();
  }

  @override
  Future<PackUnpackWeatherData> getForecastWeatherData(DayInfo dayInfo) async {
    return await getData(dayInfo);
  }


  Future<PackUnpackWeatherData> getData(DayInfo dayInfo) async{
    var weatherData;
    try{
      weatherData = await get();
      return PackUnpackWeatherData.fromJson(weatherData, dayInfo.day);
    } on HTTPCloudreadException {
      throw HTTPCloudreadException();
    }
  }

  Future<List<PackUnpackWeatherData>> getAll() async{
    var weatherData;
    List<PackUnpackWeatherData> allData = <PackUnpackWeatherData>[];
    int DAYS_NUMBER = 5;
    try{
      weatherData = await get();

      for(int i = 0; i < DAYS_NUMBER; i++){
        allData.add(PackUnpackWeatherData.fromJson(weatherData, i));
      }
      return allData;

    } on HTTPCloudreadException {
      throw HTTPCloudreadException();
    }
  }

}
