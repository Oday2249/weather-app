import 'dart:convert';

import 'package:either_dart/src/either.dart';
import 'package:weather_app/business_logic/data_models/WeatherInfo.dart';
import 'package:weather_app/business_logic/repositories/WeatherRepository.dart';
import 'package:weather_app/data_access/data_models/PackUnpackModel.dart';
import 'package:weather_app/data_access/data_source_readers/CashReader.dart';
import 'package:weather_app/data_access/data_source_readers/CloudReader.dart';
import 'package:weather_app/tools/DayInfo.dart';
import 'package:weather_app/tools/Errors.dart';

class WeatherRepositoryDef implements WeatherRepository {

  final CloudReader cloudReader;
  final Cash cash;


  WeatherRepositoryDef({
    required this.cloudReader,
    required this.cash,
  });


  @override
  Future<Either<ErrorMessage, List<WeatherInfo>>> getCurrentWeatherInfo() async {

    int res = await cash.getAvailablity(DayInfo(day: 0, date: "_"));


    if (res == 0){
      print("get current weather info from cash");
      String weatherInfo = await cash.getRegistered();
      int DAYS_NUMBER = 5;
      List<PackUnpackWeatherData> allData = <PackUnpackWeatherData>[];
      for(int i = 0; i < DAYS_NUMBER; i++){
        allData.add(PackUnpackWeatherData.fromJson(jsonDecode(weatherInfo), i));
      }
      return Right(allData);
    }else{
      try {
        print("get current weather info without cash");
        String weatherInfo = await cloudReader.getStr();

        cash.register(weatherInfo);
        int DAYS_NUMBER = 5;
        List<PackUnpackWeatherData> allData = <PackUnpackWeatherData>[];
        for(int i = 0; i < DAYS_NUMBER; i++){
          allData.add(PackUnpackWeatherData.fromJson(jsonDecode(weatherInfo), i));
        }
        return Right(allData);
      } on HTTPCloudreadException{

        return Right(PackUnpackWeatherData.createBlankList());
      }
    }


  }

  @override
  Future<Either<ErrorMessage, WeatherInfo>> getForecastWeatherInfo(DayInfo dayInfo) async {
    int res = await cash.getAvailablity(dayInfo);

    if (res == dayInfo.day) {
      print("get forecast weather info from cash");
      String weatherInfo = await cash.getRegistered();

      return Right(
          PackUnpackWeatherData.fromJson(jsonDecode(weatherInfo), res));
    } else {
      try {
        print("get forecast weather info without cash");
        String weatherInfo = await cloudReader.getStr();
        cash.register(weatherInfo);

        return Right(PackUnpackWeatherData.fromJson(
            jsonDecode(weatherInfo), dayInfo.day));
      } on HTTPCloudreadException {
        return Right(PackUnpackWeatherData.createBlank());
      } on Exception{
        return Left(ErrorMessage(message: ErrorMessages.SERVER_ERROR));
      }
    }
  }


  }