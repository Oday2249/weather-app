import 'package:weather_app/business_logic/data_models/WeatherInfo.dart';
import 'package:weather_app/presentation/states/States.dart';

class PackUnpackWeatherData extends WeatherInfo {

  PackUnpackWeatherData({
    required List<WeatherTimeInfo> weatherPerTime,
    required String date,
    required String city,

  }) : super(city: city, date: date, weatherPerTime: weatherPerTime);

  factory PackUnpackWeatherData.fromJson(Map<String, dynamic> json, int day) {
    
    int ONE_DAY_FORECASTS = 8;
    List<WeatherTimeInfo> weatherTimeInfo = <WeatherTimeInfo>[];


    print("day ${day}");
    for(int i = day * ONE_DAY_FORECASTS; i < (day + 1) * ONE_DAY_FORECASTS; i++){
      print("day -> ${i}");
      weatherTimeInfo.add(PackUnpackWeatherTimeData.fromJson(json['list'][i]));
    }
    
    return PackUnpackWeatherData(
        weatherPerTime: weatherTimeInfo,
        city: json['city']['name'],
        date: json['list'][day * ONE_DAY_FORECASTS]['dt_txt'],
    );
  }

  static PackUnpackWeatherData createBlank(){

    int ONE_DAY_FORECASTS = 8;
    List<WeatherTimeInfo> weatherTimeInfo = <WeatherTimeInfo>[];

    for(int i = 0; i < ONE_DAY_FORECASTS; i++){
      weatherTimeInfo.add(PackUnpackWeatherTimeData(time: "2021-10-10 00:00:00", tempmin: 273.15, tempmax: 273.15, state: "request error",icon: "inv"));
    }

    return new PackUnpackWeatherData(weatherPerTime: weatherTimeInfo, date: "2021-10-10 00:00:00", city: "city");
  }

  static List<PackUnpackWeatherData> createBlankList(){
    int DAYS_NUMBER = 5;
    List<PackUnpackWeatherData> BlankList = <PackUnpackWeatherData>[];
    for(int i = 0; i < DAYS_NUMBER; i++){
      BlankList.add(createBlank());
    }
    return BlankList;

  }



}

class PackUnpackWeatherTimeData extends WeatherTimeInfo {

  PackUnpackWeatherTimeData({
    required String time,
    required double tempmin,
    required double
    tempmax,
    required String state,
    required String icon,

  }) : super(time: time, tempmin: tempmin, tempmax: tempmax, state: state, icon: icon);

  factory PackUnpackWeatherTimeData.fromJson(Map<String, dynamic> json) {
    return PackUnpackWeatherTimeData(
        icon: json['weather'][0]['icon'],
        state: json['weather'][0]['main'],
        time: json['dt_txt'],
        tempmin: json['main']['temp_min'],
        tempmax: json['main']['temp_max']);
  }

}
