
import 'package:either_dart/either.dart';
import 'package:weather_app/business_logic/data_models/WeatherInfo.dart';
import 'package:weather_app/tools/DayInfo.dart';
import 'package:weather_app/tools/Errors.dart';

abstract class WeatherRepository {
  Future<Either<ErrorMessage, List<WeatherInfo>>> getCurrentWeatherInfo();
  Future<Either<ErrorMessage, WeatherInfo>> getForecastWeatherInfo(DayInfo dayInfo);
}

