import 'package:either_dart/either.dart';
import 'package:weather_app/business_logic/data_models/WeatherInfo.dart';
import 'package:weather_app/business_logic/repositories/WeatherRepository.dart';
import 'package:weather_app/tools/DayInfo.dart';
import 'package:weather_app/tools/Errors.dart';

import 'WatherUseCase.dart';

class ForecastWeatherUseCase implements UseCase<WeatherInfo> {
  final WeatherRepository repo;

  ForecastWeatherUseCase({required this.repo});

  @override
  Future<Either<ErrorMessage, WeatherInfo>> get(DayInfo dayInfo) async {
    return await repo.getForecastWeatherInfo(dayInfo);
  }
}