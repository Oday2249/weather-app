


import 'package:either_dart/either.dart';
import 'package:weather_app/business_logic/data_models/WeatherInfo.dart';
import 'package:weather_app/business_logic/repositories/WeatherRepository.dart';
import 'package:weather_app/tools/DayInfo.dart';
import 'package:weather_app/tools/Errors.dart';

import 'WatherUseCase.dart';

class CurrentWeatherUseCase implements UseCase<List<WeatherInfo>> {
  final WeatherRepository repo;

  CurrentWeatherUseCase({required this.repo});

  @override
  Future<Either<ErrorMessage, List<WeatherInfo>>> get(DayInfo neglected) async {
    return await repo.getCurrentWeatherInfo();
  }
}