import 'package:either_dart/either.dart';
import 'package:equatable/equatable.dart';
import 'package:weather_app/tools/DayInfo.dart';
import 'package:weather_app/tools/Errors.dart';


abstract class UseCase<Type> {
  Future<Either<ErrorMessage, Type>> get(DayInfo datInfo);
}
