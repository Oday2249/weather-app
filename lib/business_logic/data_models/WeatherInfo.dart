import 'package:equatable/equatable.dart';

class WeatherInfo extends Equatable {

  List<WeatherTimeInfo> weatherPerTime;

  String date;
  String city;

  WeatherInfo({
    required this.weatherPerTime,
    required this.date,
    required this.city,
  });

  @override
  List<Object> get props => [city, date, weatherPerTime];
}


class WeatherTimeInfo extends Equatable{

  String time;
  double tempmin;
  double tempmax;
  String state;
  String icon;

  WeatherTimeInfo({
    required this.time,
    required this.tempmin,
    required this.tempmax,
    required this.state,
    required this.icon
  });

  @override
  List<Object> get props => [time, tempmin, tempmax, state, icon];

}